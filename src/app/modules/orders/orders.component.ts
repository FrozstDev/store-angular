import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersModule } from './orders.module';

@Component({
  selector: 'app-orders',
  standalone: true,
  imports: [CommonModule, OrdersModule],
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export default class OrdersComponent {

}
