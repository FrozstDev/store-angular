import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { PrimeModule } from 'src/app/prime.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { DataviewComponent } from './components/dataview/dataview.component';
import { OrderlistComponent } from './components/orderlist/orderlist.component';
import { BuyerformComponent } from './components/buyerform/buyerform.component';


@NgModule({
  declarations: [
    DataviewComponent,
    OrderlistComponent,
    BuyerformComponent
  ],
  exports: [
    DataviewComponent,
    OrderlistComponent,
    BuyerformComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    PrimeModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule
  ]
})
export class OrdersModule { }
