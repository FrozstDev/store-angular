import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/app/core/environments/environment.development';
import { IOrder } from 'src/app/core/interfaces/order.interface';
import { DetailsOrder } from 'src/app/core/model/detailsOrder';
import { Product } from 'src/app/core/model/product';
import { HttpService } from 'src/app/core/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersService extends HttpService<IOrder> {

  itemsToBuy = new BehaviorSubject<DetailsOrder[]>([]);

  constructor(protected override http: HttpClient) {
    super(http, `${environment.apiUrl}/orders`)
  }

  getItems() {
    return this.itemsToBuy.asObservable();
  }

  addProduct(newProduct: Product) {
    const newDetail: DetailsOrder = new DetailsOrder()
    newDetail.product = newProduct;
    newDetail.quantity = 1;

    const currentItems = this.itemsToBuy.getValue();
    const updatedItems = [...currentItems, newDetail];
    this.itemsToBuy.next(updatedItems);
  }

  removeProduct(index: number) {
    const currentItems = this.itemsToBuy.getValue();
    currentItems.splice(index, 1);
    this.itemsToBuy.next(currentItems);
  }

  resetAllList() {
    this.itemsToBuy.next([]);
  }

  addOneQuantity(index: number, quantity: number) {
    const currentItems = this.itemsToBuy.getValue();
    const currentItem = currentItems[index];
    const updatedQuantity = currentItem.quantity + quantity;
    const updatedItem = { ...currentItem, quantity: updatedQuantity };

    const updatedItems = [...currentItems.slice(0, index), updatedItem, ...currentItems.slice(index + 1)];
    this.itemsToBuy.next(updatedItems);
  }
}
