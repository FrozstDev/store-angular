import { Product } from 'src/app/core/model/product';
import { ProductsService } from './../../../products/services/products.service';
import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrdersService } from '../../services/orders.service';

@Component({
  selector: 'app-dataview',
  templateUrl: './dataview.component.html',
  styleUrls: ['./dataview.component.scss'],
})
export class DataviewComponent {
  public products: Product[] = []

  constructor(
    private productService: ProductsService,
    private orderService: OrdersService
  ) {
  }

  ngOnInit() {
    this.loadProducts();
  }

  private loadProducts() {
    this.productService.findAll().subscribe({
      next: (response) => {
        this.products = response
      },
      error: (error: any) => {
        alert(error.message)
      }
    })
  }

  addProductToBuy(product: Product) {
    this.orderService.addProduct(product)
  }
}
