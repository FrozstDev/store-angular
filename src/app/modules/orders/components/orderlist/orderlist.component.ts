import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/core/model/product';
import { OrdersService } from '../../services/orders.service';
import { DetailsOrder } from 'src/app/core/model/detailsOrder';

@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.component.html',
  styleUrls: ['./orderlist.component.scss']
})
export class OrderlistComponent implements OnDestroy {
  items: DetailsOrder[];
  itemsService$: Subscription;

  constructor(
    private orderService: OrdersService
  ) {
    this.itemsService$ = this.orderService.getItems().subscribe({
      next: (data) => {
        this.items = data
      },
      error: (error) => {
        console.error('Error al recuperar los datos del servicio', error);
      }
    })
  }

  addOneQuantity(index: number) {
    this.orderService.addOneQuantity(index, 1)
  }

  substracOneQuantity(index: number) {
    this.orderService.addOneQuantity(index, -1)
  }

  isInRange(number: number, max: number): boolean {
    if (number === 1) {
      return true;
    } else if (number === max) {
      return true;
    } else {
      return false;
    }
  }

  ngOnDestroy(): void {
    this.itemsService$.unsubscribe()
  }
}
