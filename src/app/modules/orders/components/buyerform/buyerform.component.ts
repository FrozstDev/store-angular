import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { IOrder } from 'src/app/core/interfaces/order.interface';
import { Store } from 'src/app/core/model/store';
import { HelpersService } from 'src/app/core/services/helpers.service';
import { StoresService } from 'src/app/modules/stores/services/stores.service';
import { OrdersService } from '../../services/orders.service';
import { catchError, of, Subscription, tap } from 'rxjs';
import { messages } from 'src/app/core/constants/constants';
import { DetailsOrder } from 'src/app/core/model/detailsOrder';
import { IDetailsOrder } from 'src/app/core/interfaces/detailsOrder.interface';

@Component({
  selector: 'app-buyerform',
  templateUrl: './buyerform.component.html',
  styleUrls: ['./buyerform.component.scss'],
  providers: [MessageService, HelpersService],
})
export class BuyerformComponent implements OnDestroy {
  public formBuyer: FormGroup;
  public stores: Store[] = [];
  public submitted: boolean = false;
  items: DetailsOrder[];
  itemsService$: Subscription;

  constructor(
    private helpersService: HelpersService,
    private storesService: StoresService,
    private ordersService: OrdersService
  ) {
    this.formBuyer = this.createFormGroup();
    this.loadStores();
    this.itemsService$ = this.ordersService.getItems().subscribe({
      next: (data) => {
        this.items = data;
      },
      error: (error) => {
        console.error('Error al recuperar los datos del servicio', error);
      },
    });
  }

  ngOnInit() {
    this.loadStores();
  }

  private createFormGroup() {
    return new FormGroup({
      name: new FormControl('', [Validators.required]),
      date: new FormControl(new Date(), [Validators.required]),
      isDelivery: new FormControl(false),
      shippingAddress: new FormControl(''),
      storeId: new FormControl(''),
    });
  }

  private reset(): void {
    this.ordersService.resetAllList();
    this.formBuyer.reset();
    this.formBuyer.patchValue({
      isDelivery: false
    });

    this.submitted = false;
    this.loadStores();
  }

  public save() {
    this.submitted = true;
    if (this.formBuyer.valid) {
      this.submitCreateOrder();
    } else {
      this.helpersService.messageNotification(
        'error',
        'Verifique los campos de orden, por favor'
      );
    }
  }

  private loadStores() {
    this.storesService.findAll().subscribe({
      next: (response) => {
        this.stores = response;
      },
    });
  }

  private mappedToIDetailsOrder(
    originalItems: DetailsOrder[]
  ): IDetailsOrder[] {
    const items: IDetailsOrder[] = [];
    originalItems.map((item) => {
      const newItem: IDetailsOrder = {
        productId: item.product.id,
        quantity: item.quantity,
      };
      items.push(newItem);
    });
    return items;
  }

  private submitCreateOrder() {
    const data: IOrder = {
      name: this.formBuyer.get('name').value,
      date: this.formBuyer.get('date').value.toISOString().split('T')[0],
      isDelivery: this.formBuyer.get('isDelivery').value,
      shippingAddress: this.formBuyer.get('shippingAddress').value,
      storeId: this.formBuyer.get('storeId').value,
      items: this.mappedToIDetailsOrder(this.items),
    };

    this.ordersService
      .create(data)
      .pipe(
        tap(() => {
          this.helpersService.messageNotification(
            'success',
            messages.successCreate
          );
          this.reset();
        }),
        catchError((error) =>
          of(
            'error',
            error.map((message: any) => {
              this.helpersService.messageNotification('error', message);
            })
          )
        )
      )
      .subscribe();
  }

  getMyProductsLengthString(): string {
    return this.items.length.toString();
  }

  ngOnDestroy(): void {
    this.itemsService$.unsubscribe();
  }
}
