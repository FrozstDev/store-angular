import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsRoutingModule } from './products-routing.module';
import { ModalformsComponent } from './components/modalforms/modalforms.component';
import { PanelfilterComponent } from './components/panelfilter/panelfilter.component';
import { TableComponent } from './components/table/table.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { PrimeModule } from 'src/app/prime.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { FilterInformationComponent } from "../../shared/components/filterinformation/filterinformation.component";


@NgModule({
    declarations: [
        ModalformsComponent,
        PanelfilterComponent,
        TableComponent,
        ToolbarComponent
    ],
    exports: [
        ModalformsComponent,
        PanelfilterComponent,
        TableComponent,
        ToolbarComponent
    ],
    imports: [
        CommonModule,
        ProductsRoutingModule,
        PrimeModule,
        FormsModule,
        ReactiveFormsModule,
        PipesModule,
        FilterInformationComponent
    ]
})
export class ProductsModule { }
