import { Component } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Product } from 'src/app/core/model/product';
import { Category } from 'src/app/core/model/category';
import { TableComponent } from '../table/table.component';
import { catchError, of, tap } from 'rxjs';
import { CategoriesService } from 'src/app/modules/categories/services/categories.service';
import { HelpersService } from 'src/app/core/services/helpers.service';
import { MessageService } from 'primeng/api';
import { messages } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-modalforms',
  templateUrl: './modalforms.component.html',
  styleUrls: ['./modalforms.component.scss'],
  providers: [MessageService, HelpersService],
})
export class ModalformsComponent {
  private product: Product;
  private productResponse: Product;
  private tableComponent!: TableComponent;
  public submitted: boolean = false;
  public dialog: boolean = false;
  public formProduct: FormGroup;
  public tittleForm: string = '';
  public categories: Category[] = [];

  constructor(
    private productsService: ProductsService,
    private categoriesService: CategoriesService,
    private helpersService: HelpersService
  ) {
    this.formProduct = this.createFormGroup();
  }

  ngOnInit() {
    this.productsService.getObjectSelectedChange().subscribe((response) => {
      this.productResponse = response;
    });
    this.productsService.trigger.emit(this);
    this.productsService.triggerTable.subscribe((tableComponent) => {
      this.tableComponent = tableComponent;
    });
  }

  private createFormGroup() {
    return new FormGroup({
      id: new FormControl(null),
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      stock: new FormControl('', [Validators.required]),
      categoryId: new FormControl('', [Validators.required]),
    });
  }

  public hideDialog() {
    this.dialog = false;
    this.submitted = false;
  }

  public save() {
    this.submitted = true;
    if (this.formProduct.valid) {
      if (this.product.id) {
        this.submitUpdate(this.product.id);
      } else {
        this.submitCreate();
      }
    }
  }

  private reset(): void {
    this.formProduct.reset();
    this.product = new Product();
  }

  private submitCreate() {
    const data: Product = {
      ...this.formProduct.value,
    };
    this.productsService
      .create(data)
      .pipe(
        tap(() => {
          this.dialog = false;
          this.helpersService.messageNotification(
            'success',
            messages.successCreate
          );
          this.tableComponent.reload();
          this.reset();
        }),
        catchError((err) =>
          of(
            'error',
            err.map((message: any) => {
              this.helpersService.messageNotification('error', message);
            })
          )
        )
      )
      .subscribe();
  }

  private submitUpdate(productId: number): void {
    const data: Product = {
      ...this.formProduct.value,
    };
    this.productsService
      .update(productId, data)
      .pipe(
        tap(() => {
          this.dialog = false;
          this.helpersService.messageNotification(
            'success',
            messages.successUpdate
          );
          this.tableComponent.reload();
        }),
        catchError((err) =>
          of(
            'error',
            err.map((message: any) => {
              this.helpersService.messageNotification('error', message);
            })
          )
        )
      )
      .subscribe();
  }

  private loadCategories() {
    this.categoriesService.findAll().subscribe({
      next: (response) => (this.categories = response),
    });
  }

  public openCreate() {
    this.reset();
    this.submitted = false;
    this.tittleForm = 'Nuevo registro';
    this.loadCategories();
    this.dialog = true;
  }

  public openEdit() {
    this.tittleForm = 'Editar registro';
    if (this.productResponse && this.productResponse.id) {
      this.productsService
        .findById(this.productResponse.id)
        .pipe(
          tap((product: any) => {
            this.product = product;
            this.loadCategories();
            this.updateFormValues(product);
            this.dialog = true;
          }),
          catchError((err) =>
            of(
              'error',
              err.map((message: any) => {
                this.helpersService.messageNotification('error', message);
              })
            )
          )
        )
        .subscribe();
    } else {
      this.helpersService.messageNotification('info', 'Seleccione un equipo.');
    }
  }

  private updateFormValues(product: Product) {
    this.formProduct.patchValue(product);
    this.formProduct.patchValue({
      categoryId: product.category.id
    });
  }
}
