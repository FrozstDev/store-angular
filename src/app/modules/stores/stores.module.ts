import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoresRoutingModule } from './stores-routing.module';
import { PrimeModule } from 'src/app/prime.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { FilterInformationComponent } from 'src/app/shared/components/filterinformation/filterinformation.component';
import { PanelfilterComponent } from './components/panelfilter/panelfilter.component';
import { ModalformsComponent } from './components/modalforms/modalforms.component';
import { TableComponent } from './components/table/table.component';


@NgModule({
  declarations: [
    ToolbarComponent,
    PanelfilterComponent,
    ModalformsComponent,
    TableComponent,
  ],
  exports: [
    PanelfilterComponent,
    ToolbarComponent,
    TableComponent,
    ModalformsComponent
  ],
  imports: [
    CommonModule,
    StoresRoutingModule,
    PrimeModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    FilterInformationComponent
  ]
})
export class StoresModule { }
