import { Component, OnInit } from '@angular/core';
import { StoresModule } from './stores.module';
import { ModalinfoComponent } from 'src/app/shared/components/modalinfo/modalinfo.component';
import { ModaldeleteComponent } from 'src/app/shared/components/modaldelete/modaldelete.component';
import { Store } from 'src/app/core/model/store';
import { StoresService } from './services/stores.service';
import { tap } from 'rxjs';

@Component({
  selector: 'app-stores',
  standalone: true,
  imports: [StoresModule, ModalinfoComponent, ModaldeleteComponent],
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss']
})
export default class StoresComponent implements OnInit {
  public stores!: Store[];
  public store!: Store;

  constructor(public storesService: StoresService) {}

  ngOnInit(): void {
    this.createGrid();
    this.storesService.getObjectFilterChange().subscribe(() => {
      this.createGrid();
    });
    this.retrieveObjectSelection();
  }

  private retrieveObjectSelection() {
    this.storesService
      .getObjectSelectedChange()
      .subscribe((response: Store) => {
        this.store = response;
      });
  }

  createGrid() {
    this.storesService
      .findAll()
      .pipe(tap((items: Store[]) => (this.stores = items)))
      .subscribe();
  }
}
