import { StoresService } from './../../services/stores.service';
import { messages } from 'src/app/core/constants/constants';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from 'src/app/core/model/store';
import { TableComponent } from '../table/table.component';
import { HelpersService } from 'src/app/core/services/helpers.service';
import { catchError, of, tap } from 'rxjs';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-modalforms',
  templateUrl: './modalforms.component.html',
  styleUrls: ['./modalforms.component.scss'],
  providers: [MessageService, HelpersService],
})
export class ModalformsComponent {
  public titleForm: string = ''
  public submitted: boolean = false;
  public dialog: boolean = false;
  public messages = messages;
  public formStore: FormGroup;
  private store!: Store;
  private tableComponent!: TableComponent;
  private storeResponse: any;

  constructor(
    private storesServices: StoresService,
    private helpersService: HelpersService
  ) {
    this.formStore = this.createFormGroup();
  }

  ngOnInit() {
    this.storesServices.getObjectSelectedChange().subscribe((response) => {
      this.storeResponse = response;
    });
    this.storesServices.trigger.emit(this);
    this.storesServices.triggerTable.subscribe((tableComponent) => {
      this.tableComponent = tableComponent;
    });
  }

  private createFormGroup() {
    return new FormGroup({
      id: new FormControl('', { nonNullable: true }),
      name: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      openingHours: new FormControl('', [Validators.required]),
    })
  }

  public save() {
    this.submitted = true;
    if (this.formStore.valid) {
      if (this.store.id) {
        this.submitUpdate(this.store.id);
      } else {
        this.submitCreate();
      }
    }
  }

  private reset(): void {
    this.formStore.reset();
    this.store = new Store();
  }

  public openDialog(state: any, stateSubmitted?:any) {
    this.dialog = state;
    this.submitted= stateSubmitted;
  }

  private updateFormValues(store: Store) {
    this.formStore.patchValue(store);
  }

  public openCreate() {
    this.reset();
    this.submitted = false;
    this.titleForm = "Nuevo registro Tienda";
    this.openDialog(true);
  }

  public openEdit() {
    this.titleForm = "Editar registro Tienda";
    if (this.storeResponse && this.storeResponse.id) {
      this.storesServices
        .findById(parseInt(this.storeResponse.id))
        .pipe(
          tap((store: any) => {
            this.store = store;
            this.updateFormValues(store);
            this.openDialog(true);
          })
        )
        .subscribe();
    } else {
      this.helpersService.messageNotification('info', messages.requiredSelection);
    }
  }

  private submitCreate() {
    const data: Store = {
      ...this.formStore.value,
    };

    this.storesServices
      .create(data)
      .pipe(
        tap(() => {
          this.openDialog(false);
          this.helpersService.messageNotification('success', messages.successCreate);
          this.tableComponent.reload();
          this.reset();
        }),
        catchError(err => of('error',
          err.map((message: any) => {
            this.helpersService.messageNotification('error', message);
          })
        ))
      )
      .subscribe();
  }

  private submitUpdate(idStore: number): void {
    const data: Store = {
      ...this.formStore.value,
    };

    this.storesServices
      .update(idStore, data)
      .pipe(
        tap(() => {
          this.openDialog(false);
          this.helpersService.messageNotification('success', messages.successUpdate);
          this.tableComponent.reload();
        }),
        catchError(err => of('error',
          err.map((message: any) => {
            this.helpersService.messageNotification('error', message);
          })
        ))
      )
      .subscribe();
  }
}
