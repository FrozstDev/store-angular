import { StoresService } from './../../services/stores.service';
import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AccordionTab } from 'primeng/accordion';
import { Store } from 'src/app/core/model/store';
import { getSelectedTags } from 'src/app/core/utils/filter';

@Component({
  selector: 'app-panelfilter',
  templateUrl: './panelfilter.component.html',
  styleUrls: ['./panelfilter.component.scss']
})
export class PanelfilterComponent {
  public stores!: Store[];
  public submitted: boolean = false;
  public formStore: FormGroup;
  public selectedTags: Map<string, any> = new Map<string, any>();

  constructor(
    private storeService: StoresService
  ) {
    this.formStore = this.createFormGroup();
  }

  private createFormGroup() {
    return new FormGroup({
      name: new FormControl('', { nonNullable: true }),
      address: new FormControl('', { nonNullable: true}),
      city: new FormControl(),
      openingHours: new FormControl()
    })
  }

  public clear(): void {
    this.formStore.reset("");
    this.selectedTags.clear();
  }

  public search(tab: AccordionTab, event: MouseEvent | KeyboardEvent ) {
    const data: Store = {
      ...this.formStore.value
    };
    this.storeService.setObjectFilterChange(data);
    this.selectedTags = getSelectedTags(document, 'formStore');
    tab.toggle(event);
  }
}
