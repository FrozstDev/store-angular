import { ModalinfoComponent } from './../../../../shared/components/modalinfo/modalinfo.component';
import { Component, OnInit } from '@angular/core';
import { StoresService } from '../../services/stores.service';
import { MenuItem } from 'primeng/api';
import { ModalformsComponent } from '../modalforms/modalforms.component';
import { ModaldeleteComponent } from 'src/app/shared/components/modaldelete/modaldelete.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  private modalFormsComponent!: ModalformsComponent
  private modalInfoComponent!: ModalinfoComponent;
  private modalDeleteComponent!: ModaldeleteComponent;
  public items: MenuItem[] = [];
  public cardMenu: MenuItem[] = [];

  constructor(private storesService: StoresService) {}

  ngOnInit() {
    this.items = [
      { label: 'Duplicate', icon: 'pi pi-clone', url: 'http://angular.io' },
      { label: 'Print', icon: 'pi pi-print', routerLink: ['/theming'] },
    ]

    this.cardMenu = [
      {
        label: 'Save',
        icon: 'pi pi-fw pi-check',
      },
      {
        label: 'Update',
        icon: 'pi pi-fw pi-refresh',
      },
      {
        label: 'Delete',
        icon: 'pi pi-fw pi-trash',
      },
    ]

    this.storesService.triggerInfo.subscribe((modalInfoComponent) => {
      this.modalInfoComponent = modalInfoComponent
    })

    this.storesService.trigger.subscribe((modalFormsComponent) => {
      this.modalFormsComponent = modalFormsComponent;
    });

    this.storesService.triggerMessages.subscribe((modalDeleteComponent) => {
      this.modalDeleteComponent = modalDeleteComponent;
    });
  }

  public create() {
    this.modalFormsComponent.openCreate();
  }
  public update() {
    this.modalFormsComponent.openEdit();
  }
  public deleteSelected() {
    this.modalDeleteComponent.openConfirm();
  }
  public info() {
    this.modalInfoComponent.openInfo();
  }
}
