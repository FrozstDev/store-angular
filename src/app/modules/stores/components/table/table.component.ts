import { Table } from 'primeng/table';
import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Store } from 'src/app/core/model/store';
import { StoresService } from '../../services/stores.service';
import { messages } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent implements OnInit {
  @Input() stores!: Store[];
  @ViewChild('filter') filter!: ElementRef;
  @Output() rowSelectedEmitter = new EventEmitter<object>();
  public selectedStores: Store[] = [];
  public firstPage = 0;
  public store!: Store;
  public visible: boolean = true;
  private data = new Store();
  public messages = messages;

  constructor(private storesService: StoresService) { }

  ngOnInit() {
    this.storesService.triggerTable.emit(this);
  }


  public onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

  public clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = '';
  }

  public reload(): void {
    this.storesService.setObjectFilterChange(this.data);
    this.firstPage = 0;
  }

  public onRowSelect(event: any) {
    this.sendSelectedStore(event.data);
  }

  public onRowUnselect(event: any) {
    this.sendSelectedStore(this.data);
  }

  private sendSelectedStore(selectedStore: Store) {
    this.storesService.setObjectSelectedChange(selectedStore);
  }
}
