export const environment = {
    production: false,
    apiUrl:'http://localhost:8085/store/v1',
    formatDate: 'DD/MM/YYYY',
    formatDateDB: 'YYYY-MM-DD'
};
