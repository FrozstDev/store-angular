export const translater: Record<string, string> = {
  accessories: 'Accesorios',
  active: '¿Activo?',
  categoryName: 'Categoría',
  description: 'Descripción',
  endDate: 'Fecha de finalización',
  name: 'Nombre',
  observations: 'Observaciones',
  startDate: 'Fecha de inicio',
  address: 'Direccion',
  city: 'Ciudad',
  openingHours: 'Horario de Apertura',
};
