export interface IDetailsOrder {
  productId: number;
  quantity: number;
}
