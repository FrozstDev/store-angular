import { IDetailsOrder } from "./detailsOrder.interface";

export interface IOrder {
  name: string;
  date: string;
  isDelivery: boolean;
  shippingAddress: string;
  storeId: number;
  items: IDetailsOrder[];
}
