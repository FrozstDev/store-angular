import { Product } from "./product";

export class DetailsOrder {
  id: number = null;
  order: number = null;
  product: Product = null;
  quantity: number = 0;
}
