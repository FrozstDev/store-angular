import { Base } from "./base";

export class Order extends Base {
  id: number = null;
  name: string = '';
  date: string = '';
  shippingAddress: string = '';
  isDelivery: boolean = false;
}
