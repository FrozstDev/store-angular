import { Base } from "./base";


export class Store extends Base {
  id: number = null;
  name: string = '';
  address: string = '';
  city: string = '';
  openingHours: string = '';
}
