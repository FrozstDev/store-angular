import { Base } from './base';

export class Category extends Base {
  id: number = null;
  name: string = '';
  description: string = '';
}
