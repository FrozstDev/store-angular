export const messages: any =
{
    requiredField: 'Campo requerido.',
    requiredSelection: 'Seleccione un registro.',
    successCreate: 'Registro creado exitosamente.',
    successUpdate: 'Registro modificado exitosamente.',
    successDelete: 'Registro eliminado exitosamente.',
    select: 'Seleccione una opción...',
    emptyMessage: 'No se encontraron registros.',
    loadingRegisters: 'Cargando registros. Espere por favor.'
}
