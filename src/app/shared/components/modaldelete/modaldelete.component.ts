import { Component, Input } from '@angular/core';
import { tap, catchError, of } from 'rxjs';
import { Category } from 'src/app/core/model/category';
import { MessageService } from 'primeng/api';
import { HelpersService } from 'src/app/core/services/helpers.service';
import { CategoriesService } from 'src/app/modules/categories/services/categories.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { TableComponent } from 'src/app/modules/categories/components/table/table.component';
import { PrimeModule } from 'src/app/prime.module';
import { messages } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-modaldelete',
  templateUrl: './modaldelete.component.html',
  styleUrls: ['./modaldelete.component.scss'],
  providers: [MessageService, HelpersService],
  standalone: true,
  imports: [CommonModule, RouterModule, FormsModule, PrimeModule, PipesModule],
})
export class ModaldeleteComponent {
  @Input() serviceObject: CategoriesService | any;
  @Input() object: Category | any;
  public messages = messages;
  public dialog: boolean = false;
  private tableComponent!: TableComponent;

  constructor(
    private helpersService: HelpersService) { }

  ngOnInit() {
    this.serviceObject.getObjectSelectedChange().subscribe((response: typeof this.object) => {
      this.object = response;
    });
    this.serviceObject.triggerMessages.emit(this);
    this.serviceObject.triggerTable.subscribe((tableComponent: TableComponent) => {
      this.tableComponent = tableComponent;
    });
  }

  public openConfirm() {
    if (this.object && this.object.id) {
      this.openDialog(true);
    } else {
      this.helpersService.messageNotification('info', messages.requiredSelection);
    }
  }

  public confirmDelete() {
    this.openDialog(false);
    this.serviceObject
      .delete(parseInt(this.object.id))
      .pipe(
        tap(() => {
          this.helpersService.messageNotification('success', messages.successDelete);
          this.tableComponent.reload();
        }),
        catchError(err => of('error',
          err.map((message: any) => {
            this.helpersService.messageNotification('error', message);
          })
        ))
      )
      .subscribe();
  }

  private openDialog(state: any) {
    this.dialog = state;
  }
}
