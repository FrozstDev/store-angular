import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { PrimeModule } from 'src/app/prime.module';
import { AppFormatValueDirective } from 'src/app/core/directives/app-format-value.directive';
import { IBase } from 'src/app/core/interfaces/base.interface';
import { translater } from 'src/app/core/utils/info';
import { messages } from 'src/app/core/constants/constants';
import { HelpersService } from 'src/app/core/services/helpers.service';

@Component({
  selector: 'app-modalinfo',
  templateUrl: './modalinfo.component.html',
  styleUrls: ['./modalinfo.component.scss'],
  providers: [MessageService, HelpersService],
  standalone: true,
  imports: [CommonModule, RouterModule, FormsModule, PrimeModule, PipesModule, AppFormatValueDirective],
})
export class ModalinfoComponent {

  public dialogInfo: boolean = false;
  public object: any;
  @Input() serviceObject: any;
  @Input() objectInterface: IBase | any;
  public excludedColumns: string[] = ['id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy']
  public messages = messages;

  constructor(
    private helpersService: HelpersService) { }

  async ngOnInit() {
    await this.waitForObjectSelection();
    if(!this.objectInterface){
      this.objectInterface = this.object;
    }
  }

  private async waitForObjectSelection() {
    return new Promise<void>((resolve) => {
      this.serviceObject.getObjectSelectedChange().subscribe((response: typeof this.object) => {
        this.object = response;
        resolve();
      });
      this.serviceObject.triggerInfo.emit(this);
    });
  }

  public openInfo() {
    if (this.object && this.object.id) {
      this.dialogInfo = true;
    } else {
      this.helpersService.messageNotification('info', messages.requiredSelection);
    }
  }

  public isVariableOfType(variable: any): string {
    return typeof variable;
  }

  public translateHeader(variable: string | any): string {
    return translater[variable] ? translater[variable] : variable;
  }

  public isColumnIncluded(columnName: string | any): boolean {
    return !this.excludedColumns.includes(columnName) && !columnName.includes('Id');
  }
}
